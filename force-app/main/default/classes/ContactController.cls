public with sharing class ContactController {
    @AuraEnabled(cacheable=true)
    public static List<Contact> getContactList() {
        return [
            SELECT Id, FirstName, LastName, Title, Phone, Email
            FROM Contact
            ORDER BY FirstName ASC
            LIMIT 50
        ];
    }

    @AuraEnabled(cacheable=true)
    public static List<Contact> getOffsetContactList(Integer offset) {
        return [
            SELECT Id, FirstName, LastName, Title, Phone, Email
            FROM Contact
            ORDER BY FirstName
            LIMIT 50
            OFFSET :offset
        ];
    }
}
