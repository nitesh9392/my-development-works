import { LightningElement, wire, track, api } from 'lwc';
import getContactList from '@salesforce/apex/ContactController.getContactList';
import getOffsetContactList from '@salesforce/apex/ContactController.getOffsetContactList';

const actions = [
    { label: 'Edit Reviews', name: 'show_details' },
    { label: 'Edit Record', name: 'show_details' },
    { label: 'View', name: 'show_details' },
    { label: 'Delete', name: 'delete' }
];

const columns = [
    {
        label: 'First Name',
        fieldName: 'FirstName',
        editable: true,
        sortable: true
    },
    {
        label: 'Last Name',
        fieldName: 'LastName',
        editable: true,
        sortable: true
    },
    { label: 'Title', fieldName: 'Title', editable: true, sortable: true },
    {
        label: 'Phone',
        fieldName: 'Phone',
        type: 'phone',
        editable: true,
        sortable: true
    },
    {
        label: 'Email',
        fieldName: 'Email',
        type: 'email',
        editable: true,
        sortable: true
    },
    {
        type: 'action',
        typeAttributes: { rowActions: actions }
    }
];
export default class ApexDatatableExample extends LightningElement {
    @track error;
    @track data = [];
    @track columns = columns;
    @track record = {};
    @track sortBy;
    @track sortDirection;
    @track loadMoreStatus;
    @api totalNumberOfRows;
    @track value = 'inProgress';

    get options() {
        return [
            { label: 'New', value: 'new' },
            { label: 'In Progress', value: 'inProgress' },
            { label: 'Finished', value: 'finished' }
        ];
    }

    handleChange(event) {
        this.value = event.detail.value;
    }

    @track openmodel = false;

    openModal() {
        this.openmodel = true;
    }
    closeModal() {
        this.openmodel = false;
    }
    saveMethod() {
        //call the database method to save the records.
        this.closeModal();
    }

    wiredsObjectData;

    @wire(getContactList)
    wiredSobjects(result) {
        this.wiredsObjectData = result;
        if (result.data) {
            this.data = result.data;
        }
    }

    loadMoreData() {
        //Display a spinner to signal that data is being loaded
        //Display "Loading" when more data is being loaded
        this.loadMoreStatus = 'Loading';
        const currentRecord = this.data;
        this.tableLoadingState = true;
        const offset = currentRecord.length;

        getOffsetContactList({ offset: offset })
            .then(result => {
                const currentData = result;
                //Appends new data to the end of the table
                const newData = currentRecord.concat(currentData);

                this.data = newData;
                this.tableLoadingState = false;
                if (this.data.length >= this.totalNumberOfRows) {
                    this.loadMoreStatus = 'No more data to load';
                } else {
                    this.loadMoreStatus = '';
                }
            })
            .catch(error => {
                // eslint-disable-next-line no-console
                console.log(error);
            });
    }

    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const row = event.detail.row;
        switch (actionName) {
            case 'delete':
                this.deleteRow(row);
                break;
            case 'show_details':
                this.openModal();
                break;
            default:
        }
    }

    deleteRow(row) {
        const { id } = row;
        const index = this.findRowIndexById(id);
        if (index !== -1) {
            this.data = this.data
                .slice(0, index)
                .concat(this.data.slice(index + 1));
        }
    }

    findRowIndexById(id) {
        let ret = -1;
        this.data.some((row, index) => {
            if (row.id === id) {
                ret = index;
                return true;
            }
            return false;
        });
        return ret;
    }

    showRowDetails(row) {
        this.record = row;
    }

    doSorting(event) {
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortBy, this.sortDirection);
    }

    sortData(fieldname, direction) {
        let parseData = JSON.parse(JSON.stringify(this.data));
        // Return the value stored in the field
        let keyValue = a => {
            return a[fieldname];
        };
        // cheking reverse direction
        let isReverse = direction === 'asc' ? 1 : -1;
        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';
            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        this.data = parseData;
    }
}
